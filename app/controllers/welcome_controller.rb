require 'fileutils'
require 'java'
require 'vendor/jar/javax.mail.jar'
class WelcomeController < ApplicationController
    def index
        render json: {:status => 'running' }.to_json
    end

    def fetch_full_email
        email = EMAIL_SETTINGS
        password = PASSWORD_SETTINGS
        properties = get_properties_java
        msgOutput = 'done'
        begin
            session = javax.mail.Session.getInstance(properties, nil)
            store = session.getStore
            store.connect("imap.gmail.com", email, password);
            inbox = store.getFolder("INBOX");
            inbox.open(1); # This is read only mode for IMAP
            msgCount = inbox.getMessageCount()
            hashMsg = Hash.new
            for i in 1630..msgCount do
            #for i in 25..35 do
                msg = inbox.getMessage(i)
                strFrom = ''
                msgFrom = msg.getFrom
                msgFrom.each do |f|
                    strFrom << f.to_s
                end

                msgTo = msg.getReplyTo
                strTo = ''
                msgTo.each do |f|
                    strTo << f.to_s
                end

                mimeContent = msg.getContent
                msgContent = ''
                lengthMimes = mimeContent.getCount
                numbers_of_attachements = 0
                files = Array.new
                for j in 0..lengthMimes - 1
                    content = start_fetch_message_content(mimeContent.getBodyPart(j).getContent)
                    if content.class.name.to_s == 'Hash'
                        numbers_of_attachements += content[:nb_attachements]
                        msgContent << content[:message].to_s
                        files.concat(content[:files])
                    end
                end

                objToInsert = { :UUID => msg.getMessageNumber,
                                :FROM => strFrom,
                                :TO => strTo,
                                :SUBJECT => msg.getSubject,
                                :CONTENT => msgContent,
                                :NUMBER_OF_ATTACHEMENTS => numbers_of_attachements,
                                :FILES => files
                }
                hashMsg[i] = objToInsert
            end

        rescue Exception => e
            msgOutput = e.to_s
            msgOutput << e.backtrace.to_s
        end

        render json: {:count => msgCount, :status => msgOutput, :msg => hashMsg.to_a}.to_json
    end


    def fetch_emails_from_imap
        email = EMAIL_SETTINGS 
        password = PASSWORD_SETTINGS 
         msgOutput = 'done'
         properties = get_properties_java
        begin
            session = javax.mail.Session.getInstance(properties, nil)
            store = session.getStore
            store.connect("imap.gmail.com", email, password);
            inbox = store.getFolder("INBOX");
            inbox.open(1); # This is read only mode for IMAP
            msgCount = inbox.getMessageCount()
            hashMsg = Hash.new
            #for i in 1..msgCount do
            for i in 1..200 do
                msg = inbox.getMessage(i)
                strFrom = ''
                msgFrom = msg.getFrom
                msgFrom.each do |f|
                    strFrom << f.to_s
                end

                msgTo = msg.getReplyTo
                strTo = ''
                msgTo.each do |f|
                    strTo << f.to_s
                end

                objToInsert = { :UUID => msg.getMessageNumber,
                                :FROM => strFrom,
                                :TO => strTo,
                                :SUBJECT => msg.getSubject}
                hashMsg[i] = objToInsert
            end

        rescue Exception => e
            msgOutput = e.to_s
        end

        render json: {:count => msgCount, :status => msgOutput, :msg => hashMsg.to_a}.to_json
    end

    def fetch_folders_from_imap
        email = EMAIL_SETTINGS
        password = PASSWORD_SETTINGS
        properties = get_properties_java
        msgOutput = 'done'
        begin
            session = javax.mail.Session.getInstance(properties, nil)
            store = session.getStore
            store.connect("imap.gmail.com", email, password);

            foldersPersonnal = store.getDefaultFolder().list("*")

            folders = Hash.new
            i = 0

            foldersPersonnal.each do |f|
                folders[i] = {
                    :name => f.getName,
                }
                i += 1
            end

        rescue Exception => e
            msgOutput = e.to_s
            msgOutput << e.backtrace.join("\n")
        end

        render json: {:status => msgOutput, :folders => folders }
    end

    private

    EMAIL_SETTINGS = "insertre@cortexmedia.ca"
    PASSWORD_SETTINGS = "thisispasswd"

    STRING_TYPE = 'String'
    BASE64DECODESTREAM_CLASS = 'Java::ComSunMailUtil::BASE64DecoderStream'
    ATTACHEMENT_PREFIX = 'mail_attachmenets_'
    INVALID_BYTE_READ_STREAM = -1

    def get_properties_java
       properties = java.util.Properties.new
        properties.setProperty("mail.store.protocol", "imaps");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        return properties
    end

    def start_fetch_message_content(aMultiPartObj)
        fetch_message_content(aMultiPartObj, '', 0, Array.new)
    end

    def fetch_message_content(aMultiPartObj, strToOut, numbers_of_attachements, list_of_files)
        if aMultiPartObj.class.name == STRING_TYPE
            return generate_message_container(aMultiPartObj, numbers_of_attachements, list_of_files)
        else
            if aMultiPartObj.class.name == BASE64DECODESTREAM_CLASS
                stored_file = process_attachements(aMultiPartObj)
                numbers_of_attachements += 1
                list_of_files.push(stored_file)
                return generate_message_container('', numbers_of_attachements, list_of_files)
            else
                lengthObj = aMultiPartObj.getCount
                for k in 0..(lengthObj - 1)
                    bodyPartContent = aMultiPartObj.getBodyPart(k).getContent
                    if bodyPartContent.class.name == STRING_TYPE
                        strToOut << bodyPartContent
                        return generate_message_container(strToOut, numbers_of_attachements, list_of_files)
                    else
                        fetch_message_content(bodyPartContent, strToOut, numbers_of_attachements, list_of_files)
                    end
                end
            end
        end
    end

    def generate_message_container(aMessage, aNumberOfAttachements, aListOfFilenames)
        return {message: aMessage,
                nb_attachements: aNumberOfAttachements,
                files: aListOfFilenames}
    end

    def process_attachements(aMultiPartObj)
        filename = get_attachement_filename
        file = Java::JavaIo::File.new(filename)
        outStream = Java::JavaIo::FileOutputStream.new(file)
        buf = aMultiPartObj.read()
        while buf != INVALID_BYTE_READ_STREAM do
            outStream.write(buf)
            buf = aMultiPartObj.read()
        end
        outStream.close
        aMultiPartObj.close

        extension = identify_file_with_extension(filename)
        filename = rename_file_with_extension(filename, extension)
        return filename
    end

    def get_attachement_filename
        Rails.root.join('mail_attachements').to_s << '/' << ATTACHEMENT_PREFIX << Random.rand(1000001).to_s
    end

    def rename_file_with_extension(aFilename, aExtension)
        unix_mv = "mv " << aFilename << " " << aFilename << '.' << aExtension
        puts "runnning " << unix_mv
        system(unix_mv)

        return aFilename << '.' << aExtension
    end

    def identify_file_with_extension(aFilename)
        png = Regexp.new("\x89PNG".force_encoding("binary"))
        jpg = Regexp.new("\xff\xd8\xff\xe0\x00\x10JFIF".force_encoding("binary"))
        jpg2 = Regexp.new("\xff\xd8\xff\xe1(.*){2}Exif".force_encoding("binary"))
        case IO.read(aFilename, 10)
        when /^GIF8/
            'gif'
        when /^#{png}/
            'png'
        when /^#{jpg}/
            'jpg'
        when /^#{jpg2}/
            'jpg'
        else
            mime_type = `file #{aFilename} --mime-type`.gsub("\n", '')
            raise UnprocessableEntity, "unknown file type" if !mime_type
            mime_type.split(':')[1].split('/')[1].gsub('x-', '').gsub(/jpeg/, 'jpg').gsub(/text/, 'txt').gsub(/x-/, '')
        end
    end
end
